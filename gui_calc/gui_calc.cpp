#include <qapplication.h>

#include "calc_window.h"

int main(int argc, char **argv)
{
	QApplication app{argc, argv};
	CalculatorWindow w{};

	w.show();
	app.exec();
}