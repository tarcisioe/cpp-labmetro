#ifndef CALC_GUI_CALC_WINDOW_H
#define CALC_GUI_CALC_WINDOW_H

#include <iostream>

#include <QMainWindow>

#include "calc_logic.h"
#include "ui_calc.h"

class CalculatorWindow : public QMainWindow {
	Q_OBJECT;

public:
	CalculatorWindow() {
		ui.setupUi(this);
		connect(
			ui.equalButton, &QAbstractButton::clicked,
			this, &CalculatorWindow::onEqualClicked
		);
		connect(
			ui.zero, &QAbstractButton::clicked,
			this, &CalculatorWindow::onButtonClicked
		);
		connect(
			ui.one, &QAbstractButton::clicked,
			this, &CalculatorWindow::onButtonClicked
		);
		connect(
			ui.two, &QAbstractButton::clicked,
			this, &CalculatorWindow::onButtonClicked
		);
		connect(
			ui.three, &QAbstractButton::clicked,
			this, &CalculatorWindow::onButtonClicked
		);
		connect(
			ui.four, &QAbstractButton::clicked,
			this, &CalculatorWindow::onButtonClicked
		);
		connect(
			ui.five, &QAbstractButton::clicked,
			this, &CalculatorWindow::onButtonClicked
		);
		connect(
			ui.plusButton, &QAbstractButton::clicked,
			this, &CalculatorWindow::onButtonClicked
		);
	}

public slots:
	void onEqualClicked(bool) {
		execute();
	}

	void onButtonClicked(bool) {
		auto clicked = qobject_cast<QPushButton*>(sender());

		if (!clicked) {
			return;
		}

		auto c = clicked->text().toStdString().at(0);
		append(c);
	}

private:
	void execute() {
		std::cout << "clicked\n";
		auto input = ui.ioEdit->text().toStdString();
		auto expression = calc::logic::parse(input);
		auto result = expression.evaluate();
		ui.ioEdit->setText(QString::fromStdString(std::to_string(result)));
	}

	void append(char c) {
		auto input = ui.ioEdit->text().toStdString();
		input += c;
		ui.ioEdit->setText(QString::fromStdString(input));
	}

	Ui::CalculatorWindow ui;
};

#endif