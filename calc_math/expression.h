#pragma once
#ifndef MATH_EXPRESSION_H
#define MATH_EXPRESSION_H

#include <memory>

namespace calc {
	template <typename T>
	double eval(const T& x)
	{
		return x.evaluate();
	}

	inline double eval(double x) {
		return x;
	}

	class Expression {
	public:
		template <typename T>
		Expression(T x) :
			x{ std::make_unique<Implementation<T>>(x) }
		{}

		Expression(const Expression& other) :
			x{ other.x->copy() }
		{}

		double evaluate() const {
			return x->evaluate_();
		}

	private:
		struct Model {
			virtual ~Model() = default;
			virtual std::unique_ptr<Model> copy() const = 0;
			virtual double evaluate_() const = 0;
		};

		template <typename T>
		struct Implementation : Model {
			Implementation(T x) :
				x{ x }
			{}

			std::unique_ptr<Model> copy() const override
			{
				return std::make_unique<Implementation>(*this);
			}

			double evaluate_() const override
			{
				return eval(x);
			}

			T x;
		};

		std::unique_ptr<Model> x;
	};

}

#endif