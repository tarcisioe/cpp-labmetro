#pragma once
#ifndef MATH_MATH_H
#define MATH_MATH_H

namespace calc::math {
	double power(double, int);
	double sqrt(double);
}

#endif