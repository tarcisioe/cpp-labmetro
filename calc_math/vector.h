#pragma once
#ifndef MATH_VECTOR_H
#define MATH_VECTOR_H

namespace calc::math {
	class Vector {
	public:
		Vector(double x, double y) :
			x_{x},
			y_{y}
		{}

		Vector() = default;

		double x() const;
		double y() const;

		void invert();
		Vector& operator*=(double);
		Vector& operator+=(const Vector&);

	private:
		double x_{0}, y_{0};
	};

	Vector operator-(Vector);
	Vector operator*(Vector, double);
	Vector operator*(double, Vector);
}

#endif