#include "math.h"

namespace calc::math {
	double power(double x, int e)
	{
		if (e == 0) {
			return 1;
		}

		auto half_e = e / 2;
		auto half_power = power(x, half_e);
		auto result = half_power*half_power;

		if (e % 2 == 1) {
			result *= x;
		}

		return result;
	}

	double sqrt(double x)
	{
		auto old = 0.0;
		auto guess = 1.0;

		while (old != guess) {
			old = guess;
			guess = (x / guess + guess) / 2;
		}

		return guess;
	}
}