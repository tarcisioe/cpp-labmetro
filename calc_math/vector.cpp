#include "vector.h"

using namespace calc::math;

double calc::math::Vector::x() const
{
	return x_;
}

double calc::math::Vector::y() const
{
	return y_;
}

void calc::math::Vector::invert()
{
	x_ = -x_;
	y_ = -y_;
}

Vector& calc::math::Vector::operator*=(double s)
{
	x_ *= s;
	y_ *= s;
	return *this;
}

Vector& calc::math::Vector::operator+=(const Vector& other)
{
	x_ += other.x_;
	y_ += other.y_;
	return *this;
}

Vector calc::math::operator-(Vector v)
{
	v.invert();
	return v;
}

Vector calc::math::operator*(Vector v, double s)
{
	v *= s;
	return v;
}

Vector calc::math::operator*(double s, Vector v)
{
	return v * s;
}