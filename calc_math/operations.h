#pragma once
#ifndef MATH_OPERATIONS_H
#define MATH_OPERATIONS_H

#include "expression.h"
#include "calc_math.h"

namespace calc::operations {

struct Add {
	const Expression lhs, rhs;
	double evaluate() const {
		return lhs.evaluate() + rhs.evaluate();
	}
};

struct Sub {
	const Expression lhs, rhs;
	double evaluate() const {
		return lhs.evaluate() - rhs.evaluate();
	}
};

struct Mul {
	const Expression lhs, rhs;
	double evaluate() const {
		return lhs.evaluate() * rhs.evaluate();
	}
};

struct Div {
	const Expression lhs, rhs;
	double evaluate() const {
		return lhs.evaluate() / rhs.evaluate();
	}
};

struct Sqrt {
	const Expression e;
	double evaluate() const {
		return calc::math::sqrt(e.evaluate());
	}
};

}

#endif