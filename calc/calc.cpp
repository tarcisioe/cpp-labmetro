#include <algorithm>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>
#include <optional>

#include "calc_logic.h"

using namespace std::string_literals;

using namespace calc;
using namespace calc::logic;

struct EndOfFileError : std::exception {};

std::string get_input(std::istream &in, const std::string &prompt = ""s)
{
	std::cout << prompt;
	auto input = ""s;
	std::getline(in, input);

	if (std::cin.eof()) {
		throw EndOfFileError{};
	}

	return input;
}

bool execute(const std::string &input) {
	if (input == "exit"s) {
		return false;
	}

	auto expr = parse(input);
	std::cout << expr.evaluate() << '\n';
	return true;
}

void calculator_loop()
{
	auto running = true;

	while (running) {
		auto input = get_input(std::cin, "> "s);
		try {
			running = execute(input);
		} catch (const SyntaxError&) {
			std::clog << "Syntax error.\n";
		}
	}
}

int main()
{
	try {
		calculator_loop();
	} catch (const EndOfFileError&) {
		std::exit(0);
	} catch (...) {
		std::clog << "Unknown error.\n";
		std::abort();
	}
}
