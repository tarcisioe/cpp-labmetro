#ifndef CALC_LOGIC_CALC_LOGIC_H
#define CALC_LOGIC_CALC_LOGIC_H

#include <string_view>
#include <stdexcept>

#include "expression.h"

namespace calc::logic {
	struct SyntaxError : std::exception {};
	struct UnexpectedCharacter : SyntaxError {};

	Expression parse(std::string_view v);
}

#endif